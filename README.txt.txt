Projekt aplikacji webowej kina. Aplikacja wy�wietla podstawowe informacje o najbli�szych seansach oraz aktualnie granych filmach.
Posiada r�wnie� mo�liwo�� zarejestrowania si�, a tak�e mo�liwo�� zakupu biletu na konkretny seans, co skutkuje wys�aniem do zarejestrowanego
u�ytkownika wygenerowanego pliku PDF na jego adres email. 

Uruchomienie aplikacji:

W celu uruchomienia aplikacji nale�y za�o�y� swoj� baz� danych MySQL (przyk�adowo w programie MySQLWorkbench)
oraz wyedytowa� plik hibernate.properties znajduj�cy si� w podkatalogu src/main/resources zgodnie ze stworzon� baz�.
Po utworzeniu bazy nale�y uruchomi� aplikacje w odpowiednim IDE np Spring Tool Suite i wpisa� po g��wnej cz�ci linku "/welcome/test"
Przyk�adowo przy uruchomieniu aplikacji na lokalnym komputerze:
http://localhost:8080/cinema/welcome/test
Utworzy to wszystkie tabele potrzebne do poprawnego dzia�ania aplikacji.
Nast�pnie nale�y utworzy� dane w bazie danych. Mo�na to zrobi� za pomoc� plik�w dostepnych w podkatalogu: SQLQueries

Program jest gotowy do uruchomienia.

Przegl�d aplikacji:
Wst�pny przegl�d aplikacji w formie filmu znajduje si� w podkatalogu: Sample