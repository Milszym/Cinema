package pl.cinema.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.cinema.domain.Seanse;
import pl.cinema.domain.SimpleMail;
import pl.cinema.pdf.MailWithPdf;
import pl.cinema.pdf.PdfTicketBuilder;
import pl.cinema.service.SeanseService;

@Controller
@RequestMapping("/tickets")
public class TicketController {
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private SeanseService seanseService;

	@RequestMapping("")
	public String tickets(Model model){
		
		model.addAttribute("isSeanse", 0);
		
		return "tickets";
	}
	
	//przekierowanie ze strony seans�w do kupowania biletu, w PathVariable przechowywane jest id seansu na kt�ry u�ytkownik chce zakupi� bilet
	@RequestMapping("{seanseId}")
	public String ticketsBuying(@PathVariable("seanseId") String seanseId, Model model){
		
		Seanse seanseForTicket = seanseService.getSeanseById(Integer.parseInt(seanseId));
		
		SimpleMail mailForPdf = new SimpleMail();
		
		model.addAttribute("isSeanse", 1);
		model.addAttribute("seanseForTicket", seanseForTicket);
		model.addAttribute("mail", mailForPdf);
		
		return "tickets";
	}
	
	//przekierowanie z formularza za��czonego na stronie zakupu biletu
	//po zakupie generowany jest pdf, kt�ry nastepnie jest wysy�any do u�ytkownika na maila kt�ry poda� w formularzu
	@RequestMapping(value="/{seanseId}/buy", method=RequestMethod.POST)
	public String downloadTicketAsPdf(@ModelAttribute("mail") SimpleMail mail, HttpServletResponse response, @PathVariable("seanseId") String seanseId, Model model){
		
		Seanse seanseForTicket = seanseService.getSeanseById(Integer.parseInt(seanseId));
		
		String pdfId = new String("2");
		
		PdfTicketBuilder ptb = new PdfTicketBuilder();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		try {
			pdfId = ptb.createPdf(seanseForTicket, seanseForTicket.getMovie(), auth.getName().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		
		System.out.println(mail.getMail());
/*		ApplicationContext context =
	            new ClassPathXmlApplicationContext("Simple-Mail.xml");*/
//		ApplicationContext context = 
//				new ClassPathXmlApplicationContext("pl/cinema/controller/Simple-Mail.xml");

		
		MailWithPdf mwp= (MailWithPdf) context.getBean("mailMail");
		
		mwp.sendMailAnotherWay(auth.getName().toString(), ", oto Tw�j bilet!", seanseId, mail.getMail(), pdfId);
		
		return "tickets";
	}
	

	
	
}
