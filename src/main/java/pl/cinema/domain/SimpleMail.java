package pl.cinema.domain;

public class SimpleMail {

	private String mail;
	private String confirmMail;
	
	public SimpleMail(){
		
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getConfirmMail() {
		return confirmMail;
	}
	public void setConfirmMail(String confirmMail) {
		this.confirmMail = confirmMail;
	}
	
}
