package pl.cinema.pdf;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import pl.cinema.domain.Seanse;
 
/**
 * This view class generates a PDF document 'on the fly' based on the data
 * contained in the model.
 * @author www.codejava.net
 *
 */
public class PDFBuilder extends AbstractTextPdfView {
 

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document doc,
            PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // get data model which is passed by the Spring container
        List<Seanse> listSeanses = (List<Seanse>) model.get("listBooks");
         
       
         
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {3.0f, 2.0f, 2.0f, 2.0f, 1.0f});
        table.setSpacingBefore(10);
         
        // define font for table header row
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);
         
        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setPadding(5);
         
        // write table header
        cell.setPhrase(new Phrase("Tytu� filmu", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("D�ugo�� trwania", font));
        table.addCell(cell);
 
        cell.setPhrase(new Phrase("Godzina seansu", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Published Date", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Price", font));
        table.addCell(cell);
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
        // write table row data
        for (Seanse aSeanse : listSeanses) {
        	doc.add(new Paragraph("Szanowny Panie "+auth.getName()+" oto Pa�ski bilet."));
            table.addCell(aSeanse.getMovie().getPolishTitle());
            table.addCell(String.valueOf(aSeanse.getMovie().getLength()));
            table.addCell(String.valueOf(aSeanse.getTime()));
            table.addCell(String.valueOf(aSeanse.getDate()));
            table.addCell(String.valueOf(aSeanse.getPrice()));
        }
         
        doc.add(table);
         
    }
 
}