package pl.cinema.pdf;

import java.io.FileOutputStream;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;

import pl.cinema.domain.Movie;
import pl.cinema.domain.Seanse;


//klasa u�ywana do tworzenia pdfu z informacjami na temat zakupionego biletu
public class PdfTicketBuilder {
	
	public static int nextId;
	
	public PdfTicketBuilder(){
		nextId=0;
	}
	
	public String createPdf(Seanse seanse, Movie movie, String userName) throws Exception{
		
		String pdfId = String.valueOf(seanse.getSeanseId())+nextId;
		
		Document document = new Document();
		PdfWriter.getInstance(document,  new FileOutputStream("C:/Szczypiorek/"+pdfId+"sytuacja.pdf"));
		//PdfWriter.getInstance(document,  new FileOutputStream("file:src/main/webapp/resources/pdf/"+nextId+"sytuacja.pdf"));
		document.open();
		document.addTitle("Bilet");
		document.add(new Paragraph("Hej "+userName+" , oto Tw�j bilet."));

		document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
		
		DottedLineSeparator dottedline = new DottedLineSeparator();
        dottedline.setOffset(-2);
        dottedline.setGap(2f);
        Paragraph p = new Paragraph("");
        p.add(dottedline);
        document.add(p);
        
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
		
		PdfPTable table = new PdfPTable(5);
		PdfPCell cell;
		
		int s = 5;
		String[] headers = {"Tytul polski", "Data", "Godzina", "Dlugosc", "Rezyser"};
		
		for(int i=0;i<s;i++){
			cell = new PdfPCell(new Paragraph(headers[i]));
			cell.setColspan(1);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPadding(10.0f);;
			table.addCell(cell);
		}
		
		String[] data = new String[s];
		data[0] = movie.getPolishTitle();
		data[1] = seanse.getDate().toString();
		data[2] = seanse.getTime().toString();
		data[3] = String.valueOf(movie.getLength());
		data[4] = movie.getDirector();
		for(int i=0;i<s; i++){
			cell = new PdfPCell(new Paragraph(data[i]));
			cell.setColspan(1);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPadding(2.0f);;
			table.addCell(cell);
		}
		

		document.add(table);
		document.close();
		
		nextId++;
		
		
		
		return pdfId;
	}

}
