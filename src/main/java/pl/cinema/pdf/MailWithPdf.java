package pl.cinema.pdf;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailWithPdf {
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(String dear, String content, String seanseId, String mailTo) {

		MimeMessage message = mailSender.createMimeMessage();
		

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
/*			simpleMailMessage.setTo(mailTo);
			helper.setFrom(simpleMailMessage.getFrom());
			// helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(simpleMailMessage.getSubject());
			helper.setText(String.format(simpleMailMessage.getText(), dear, content));*/
			simpleMailMessage.setTo(mailTo);
			helper.setFrom("niebieskonogi@gmail.com");
			// helper.setTo(simpleMailMessage.getTo());
			helper.setSubject("Sytuacja :)");
			helper.setText(dear+ content);

			FileSystemResource file = new FileSystemResource("C:/Szczypiorek/2sytuacja.pdf");
			helper.addAttachment(file.getFilename(), file);
			
			
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
	
	public void sendMailAnotherWay(String dear, String content, String seanseId, String mailTo, String pdfId){
		JavaMailSenderImpl mailLdsaSender = new JavaMailSenderImpl();
		mailLdsaSender.setHost("smtp.gmail.com");
		mailLdsaSender.setPort(587);

		mailLdsaSender.setUsername("niebieskonogi@gmail.com");

		mailLdsaSender.setPassword("5tgb^YHN");
		Properties prop = mailLdsaSender.getJavaMailProperties();
		prop.put("mail.transport.protocol", "smtp");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.debug", "true");
		
	   MimeMessage mimeMessage = mailLdsaSender.createMimeMessage();
      	   
      	   try{
      		 MimeMessageHelper mailMsg = new MimeMessageHelper(mimeMessage, true);
      	   mailMsg.setFrom("niebieskonogi@gmail.com");
      	   mailMsg.setTo(mailTo);
      	   mailMsg.setSubject(dear);
      	   mailMsg.setText(dear+content);
      	   FileSystemResource file = new FileSystemResource("C:/Szczypiorek/"+pdfId+"sytuacja.pdf");
      	   mailMsg.addAttachment(file.getFilename(), file);
      	   }catch(Exception e){}
	   mailSender.send(mimeMessage);
	   System.out.println("---Done---");
	}
	

}
